//
//  PreviewViewController.swift
//  TCS_DemoAssignment
//
//  Created by Anjali singh on 07/04/19.
//  Copyright © 2019 anjali. All rights reserved.
//

import UIKit

class PreviewViewController: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var previewImgView: UIImageView!
    var imageURL : NSString = ""
    var view_title: NSString = ""

    @IBOutlet weak var loaderOutlet: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLbl.text = view_title as String
        let url = URL(string: imageURL as String)
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            if data != nil {
                DispatchQueue.main.async {
                    self.previewImgView.image = UIImage(data: data!)
                    self.loaderOutlet.isHidden = true
                }
            }

        // Do any additional setup after loading the view.
    }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

     // MARK: - Button Action
    @IBAction func closeAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    



}
