//
//  GraphViewController.swift
//  TCS_DemoAssignment
//
//  Created by Anjali singh on 08/04/19.
//  Copyright © 2019 anjali. All rights reserved.
//

import UIKit
import SwiftCharts

class GraphViewController: UIViewController {


    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var baseBarBackground: UIView!

    @IBOutlet weak var baseLineBackground: UIView!
    var chartConfig : BarsChartConfig!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        self.barGraph()
        self.baseBarBackground.isHidden = false
        self.baseLineBackground.isHidden = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

        // MARK: - Segment Controls
    @IBAction func graphTypeSegment(_ sender: UISegmentedControl) {

        print("selected segment:\(sender.tag)")

            self.titleLbl.text = sender.titleForSegment(at: sender.selectedSegmentIndex)

        if  sender.selectedSegmentIndex == 0{
            self.barGraph()
            self.baseBarBackground.isHidden = false
            self.baseLineBackground.isHidden = true
        }else {
            self.Linegraph()
            self.baseBarBackground.isHidden = true
            self.baseLineBackground.isHidden = false
        }


    }

    // MARK: - Graph Methods
    /*
     *  Method to draw chart with line representation
     *  Todo :-  Name on axis not visible
     */
    func Linegraph()  {
        for view in self.baseLineBackground.subviews {
            view.removeFromSuperview()
        }

        let chartConfig = ChartConfigXY(
            xAxisConfig: ChartAxisConfig(from: 2, to: 14, by: 2),
            yAxisConfig: ChartAxisConfig(from: 0, to: 14, by: 2)
        )

        let frame = CGRect(x: 0, y: 0, width:self.baseLineBackground.frame.width-80, height: 400)

        let chart = LineChart(
            frame: frame,
            chartConfig: chartConfig,
            xTitle: "city",
            yTitle: "value",
            lines: [
                (chartPoints: [(2.0, 10.6), (4.2, 5.1), (7.3, 3.0), (8.1, 5.5), (14.0, 8.0)], color: UIColor.red),
                (chartPoints: [(2.0, 2.6), (4.2, 4.1), (7.3, 1.0), (8.1, 11.5), (14.0, 3.0)], color: UIColor.blue)
            ]
        )

        self.baseLineBackground.addSubview(chart.view)
    }

/*
 *  Method to draw chart with bar representation
*  Todo :-  Name on axis not visible
*/
    func barGraph()  {
        for view in self.baseBarBackground.subviews {
            view.removeFromSuperview()
        }
        chartConfig = BarsChartConfig(
            valsAxisConfig: ChartAxisConfig(from: 0, to: 100, by: 20)
        )

        let frame = CGRect(x: -10, y: 0, width: self.baseBarBackground.frame.width-80, height: 400)

        let chart = BarsChart(
            frame: frame,
            chartConfig: chartConfig,
            xTitle: "city",
            yTitle: "value",
            bars: [
                ("A", 24),
                ("B", 91),
                ("C", 81),
                ("D", 40),
                ("E", 52),
                ("F", 28)
               /* ("Interlochen", 24),
                ("Falconaire", 91),
                ("Gardiner", 81),
                ("Tibbie", 40),
                ("Southmont", 52),
                ("Frizzleburg", 28)*/
            ],
            color: UIColor.gray,
            barWidth: 20
        )

        self.baseBarBackground.addSubview(chart.view)
    }




}
