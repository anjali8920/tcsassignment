//
//  gallaryModel.swift
//  TCS_DemoAssignment
//
//  Created by Anjali singh on 08/04/19.
//  Copyright © 2019 anjali. All rights reserved.
//

import Foundation

struct gallaryModel {
    var id : Int?
    var author: NSString?
    var authorUrl: NSString?
    var postUrl: NSString?
}
