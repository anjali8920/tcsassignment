//
//  GallaryRestApiCall.swift
//  TCS_DemoAssignment
//
//  Created by Anjali singh on 08/04/19.
//  Copyright © 2019 anjali. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

var alamoFireManager : SessionManager?

@objc protocol GallaryRestApiCallDelegate {
    func gallaryDetailsReceivedSuccessFullly(data: NSMutableArray)
    func failedToCommunicateWithDevice(error: Error)
}
struct GallaryRestApiCalls {

    var delegate : GallaryRestApiCallDelegate?


    func callgallaryApi() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 15
        configuration.timeoutIntervalForResource = 15
        alamoFireManager = Alamofire.SessionManager(configuration: configuration)
         Alamofire.request(TCS_DemoAssistmentURLs.tcsAppBaseURL, method: .get, headers:nil).responseData { (response) in
            switch response.result {
            case .success(let _):

                print("****** SUCCESS saveWiFiDetails *********")
                let deviceResponse : JSON = JSON(response.result.value!)
                print("JSON: \(deviceResponse)")
               // var gallaryList = [gallaryModel]()
                let gallaryList = NSMutableArray ()
                //let actualList = value as! Array<Any>
                for item in deviceResponse {
               var gallaryDetails = gallaryModel()

                    let json :JSON = JSON(item.1)
                    gallaryDetails.id = json["id"].intValue
                    gallaryDetails.postUrl = json["post_url"].stringValue as NSString
                    gallaryDetails.author = json["author"].stringValue as NSString
                  //  gallaryList.append(gallaryDetails)
                    gallaryList.add(gallaryDetails)
                }
                self.delegate?.gallaryDetailsReceivedSuccessFullly(data: gallaryList)
            case .failure(_):
                print("Error loging in: \(String(describing: response.error))")
            }
           }
        }
    }
    
//}

