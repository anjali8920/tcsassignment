//
//  PersistentData.swift
//  TCS_DemoAssignment
//
//  Created by Anjali singh on 07/04/19.
//  Copyright © 2019 anjali. All rights reserved.
//

import Foundation

struct TCS_DemoAssistmentURLs {
    static let  tcsAppBaseURL = "https://picsum.photos/list"
    static let tcsgallaryImgBaseURL = "https://picsum.photos/200?image="
    static let tcspreviewBaseURL = "https://picsum.photos/500?image="
}
