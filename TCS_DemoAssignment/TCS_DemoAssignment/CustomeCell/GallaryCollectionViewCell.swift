//
//  GallaryCollectionViewCell.swift
//  TCS_DemoAssignment
//
//  Created by Anjali singh on 06/04/19.
//  Copyright © 2019 anjali. All rights reserved.
//

import UIKit

class GallaryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var photoAuthorLbl: UILabel!
    
    @IBOutlet weak var loaderIndicator: UIActivityIndicatorView!
}
