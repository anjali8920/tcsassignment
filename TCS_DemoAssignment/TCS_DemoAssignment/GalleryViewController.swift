//
//  GalleryViewController.swift
//  TCS_DemoAssignment
//
//  Created by Anjali singh on 07/04/19.
//  Copyright © 2019 anjali. All rights reserved.
//

import UIKit

class GalleryViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    @IBOutlet weak var gallaryCollectionView: UICollectionView!
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    var selectedURL: NSString!
    var selectedTitle: NSString!
    var gallaryListArray = NSMutableArray()
    
     override func viewDidLoad() {

        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height

        super.viewDidLoad()

        var callgallary_Api = GallaryRestApiCalls()
        callgallary_Api.delegate = self
        callgallary_Api.callgallaryApi()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

        // MARK: - UICollection delegate and datasource

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gallaryListArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath) as! GallaryCollectionViewCell
        let gallaryType = gallaryListArray[indexPath.row]
        let num =  (gallaryType as! gallaryModel).id
        cell.photoAuthorLbl.text = (gallaryType as! gallaryModel).author! as String

        let url = URL(string: TCS_DemoAssistmentURLs.tcsgallaryImgBaseURL + "\(num ?? 0)")
        cell.photoImageView.image = nil
        cell.loaderIndicator.isHidden = false
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            if data != nil {
            DispatchQueue.main.async {
                cell.photoImageView.image = UIImage(data: data!)
                cell.loaderIndicator.isHidden = true
            }
            }
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let gallaryType = gallaryListArray[indexPath.row]
        let num =  (gallaryType as! gallaryModel).id
        self.selectedURL = TCS_DemoAssistmentURLs.tcspreviewBaseURL + "\(num ?? 0)" as NSString
        self.selectedTitle = (gallaryType as! gallaryModel).author!
        self.performSegue(withIdentifier: "fullViewSegue", sender: self)
    }


    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let size = UIScreen.main.bounds.size
        return CGSize(width: (size.width / 3) - 16, height: (size.width / 3) - 45)

    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "fullViewSegue" {
            let navigationController = segue.destination as! PreviewViewController
            //let controller = navigationController.topViewController as! PreviewViewController
            navigationController.view_title = self.selectedTitle
            navigationController.imageURL = selectedURL
        }
    }


}

    // MARK: - API delegates'
extension GalleryViewController : GallaryRestApiCallDelegate {
    func gallaryDetailsReceivedSuccessFullly(data: NSMutableArray) {
        print("Success data: \(data)")
        gallaryListArray = data.mutableCopy() as! NSMutableArray
        gallaryCollectionView.reloadData()
    }

    func failedToCommunicateWithDevice(error: Error) {
         print("Failure")
    }



}
